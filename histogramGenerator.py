import scipy.signal as sig
import pandas as pd
import polars as pl
import numpy as np
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import multiprocessing as mp
import glob

DISTANCE = 1000
PROMINENCE = 4
HEIGHT = -135

def onlyPeaks(values, distance, prominence, height):
    return sig.find_peaks(values, distance = DISTANCE, prominence = PROMINENCE, height = HEIGHT)[0]

dataDir = '/home/ivan/Gitlab/iaea-pmt/data/Co60/splitFiles/test/Co60CSV/'
fileExtension = "csv"



dataDir = dataDir.rstrip("/") + '/'
fileList = glob.glob(dataDir + "*." + fileExtension)
print(str(len(fileList)) + " files found")


allHistValues = []
nFiles = len(fileList)



def findPeaksMP(singleFile, queue):
    hVal = []
    dfChunk = pd.read_csv(singleFile)
    dfChunk = dfChunk[:]*(-1)
    dfChunk ["peakIndexes"] = dfChunk[dfChunk.columns[:-1]].apply(onlyPeaks, args = (DISTANCE, PROMINENCE, HEIGHT), axis = 1)
    
    for j in range(len(dfChunk)):
        singlePulse = dfChunk.loc[j]
        yPeaks = singlePulse.T[singlePulse["peakIndexes"]].to_list()
        hVal.extend(yPeaks)
    queue.put(hVal)


nThreads = 10
nFiles = 10

mpQueues = [mp.Queue() for i in range(nThreads)]
 
for i in range(0, nFiles, nThreads):
    for j in range(nThreads):
        currentFileIndex = i + j
        if currentFileIndex < nFiles:
            p = mp.Process(target = findPeaksMP, args = (fileList[currentFileIndex], mpQueues[j]))
            p.start()  
            print("Processing file " + str(currentFileIndex + 1) + "/" + str(nFiles))
    for k in range(nThreads):
        allHistValues.extend(mpQueues[k].get())
        p.join()

print(len(allHistValues))

sns.histplot(allHistValues, bins = -HEIGHT)
plt.show()
