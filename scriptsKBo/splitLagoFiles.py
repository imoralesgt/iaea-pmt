import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import sys

if len(sys.argv) <5:
    print("Error, missing arguments")
    sys.exit()

sdir=sys.argv[1].rstrip('/') #Source Directory
obase=sys.argv[2].rstrip('/') #Output Directory

if sys.argv[3] == '':
    csize=5000 #Chunk size
else:
    csize=int(sys.argv[3])
    print("Chunk Size", csize)

if sys.argv[4] == '':
    nchunks=100
else:
    nchunks=int(sys.argv[4])
    print("Nchunks ",nchunks,"\n")


flist=os.listdir(sdir)

##Check if directory exists
if os.path.isdir(obase)==False:
    os.mkdir(obase)

for lfile in flist:
##Creating folder for each .lag file
    print("Open ",sdir+'/'+lfile,"file")
    f=open(sdir+'/'+lfile, 'r')
    odir=obase+'/'+lfile.strip('.lag')
    print("To store in: ",odir,'\n')
    os.mkdir(odir)

    it=0
    cname=""
    f.seek(0) ##To be sure to read from the first line


    while(True):
        print("Starting iteration ",it)
        df=pd.DataFrame() ##Create DataFrame
        data=[] 
        i=0
        while(True):
            l=f.readline()
            if l == '' or i==csize:
                break
            elif "T" in l:
                if i>0:
                    df[cname]=data
                cname=l.strip()
                data=[]
                i=i+1
                if i%(csize/10)==0:
                    print(i,' ')
            else:
                data.append(int(l.strip()))
        cname=odir+'/'+lfile.strip('.lag')+"_"+str(it)+".csv"
        df.to_csv(cname)
        print("File ",cname,"created. \n")
        it+=1
        if l == '' or i==(nchunks*csize):
            break
print("Done \n")

