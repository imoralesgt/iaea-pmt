import glob
import pandas as pd

PATH = "/mnt/hdd500/cernbox/Na22_v4_t100_s65530_hv2000_noPb_v2/20210830/woPb_split/20210830_124918"
EXTENSION = "*.csv"
OUTPUT_NAME = "/mnt/hdd500/cernbox/Na22_v4_t100_s65530_hv2000_noPb_v2/20210830/woPb_split/20210830_124918/Na22_v4_t100_noPb_joined.csv"

path = PATH.rstrip("/")

individualFiles = glob.glob(PATH + "/" + EXTENSION)


destDf = pd.DataFrame()

print("Found " + str(len(individualFiles)) + " files")

for i in individualFiles:
    tempDf = pd.read_csv(i, index_col = 0)
    
    print("Processing file: " + str(i))
    
    destDf = pd.concat([destDf, tempDf], axis = 1, ignore_index = True)

print("Reading files done. Writing output file...")

destDf.to_csv(OUTPUT_NAME)

print("Everything done!")