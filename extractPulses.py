#!/usr/bin/python3

import glob
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

'''
============================
          START
User-configurable parameters
============================
'''

#Files I/O
# INPUT_DIR = '/mnt/hdd500/cernbox/Co60_v4_t100_s65534_hv2000/20210825'
INPUT_DIR = '/home/ivan/tmp/AllCo60/'
INPUT_FILE_EXTENSION = '.lag'

INDIVIDUAL_PULSES_FILE_NAME = './PeakDetection_test_Co60Reduced.csv'
AREAS_FILE_NAME = './PeakDetection_test_Co60Reduced_Areas.csv'


# CURRENT_RADIONUCLEID = "Na22"
CURRENT_RADIONUCLEID = "Co60"
# CURRENT_RADIONUCLEID = "Cs137"

'''
Pulse-isolating algorithm
Looking only for very isolated pulses in time
'''
#Samples to discard before pulse baseline computation
DEAD_SAMPLES_BEFORE_TRIGGER = 10

#Samples to average to compute baseline
BASELINE_AVERAGE_SAMPLES = 50

#Samples per pulse
PMT_PULSE_LENGTH = 1500


#Samples to measure a continuous positive derivative
# to determine trigger condition -> pulse
CONTINUOUS_POSITIVE_DERIVATIVES = 3

#Distance between current and previous pulse triggers
# to determine if the current pulse is far enough from
# last pulse
SAMPLES_BETWEEN_PULSES = 2000

#Pulse polarity (default is negative). May be
# either -1 or +1 (negative/positive)
SIGNAL_POLARITY = -1
DATA_BITS = 8

#Max baseline deviation to avoid spurious small peaks within
#baseline computation. Must be a small number between 1 and 3
MAX_BL_DEVIATION = 1.25


#Peak detection: peak threshold (absolute) minimum
PK_THRSH_ABS = 4

#Peak detection: peak threshold (relative) minimum
PK_HEIGHT = 2


#Filtering individual pulses by their waveform, based on how peaks are distributed
#At least, 10 pulses within the initial window
PK_DISTR_MIN_PULSES = 5

#Window to look for pulse count
PK_DISTR_WINDOW_SIZE = 300

'''
Processing-related parameters
'''
DROP_SATURATED_PULSES = True


'''
============================
User-configurable parameters
           END
============================
'''



'''
============================
         CONSTANTS
============================
'''
SATURATION_VALUE = 0
COMMA_SEPARATOR = ','
CR_SEPARATOR = '\n'
SATURATION_STRING = COMMA_SEPARATOR + str(SATURATION_VALUE)
PULSE_HEADER = 'TBP'
PMT_PULSE_LENGTH -= (DEAD_SAMPLES_BEFORE_TRIGGER + BASELINE_AVERAGE_SAMPLES)




'''

============================
    PANDAS DF CONSTANTS
============================
'''
PEAK_LOCATIONS_COLUMN_NAME = "peakLocations"





#Return a list of files to process
def getFilesList(path, extension):
    path = path.rstrip('/') + "/"
    extension = extension.lstrip('.')
    extension = '.' + extension
    
    listOfFiles = glob.glob(path + "*" + extension)    
    
    return listOfFiles


def derivative(values):
    return values[-1] - values[-2]



#Determine the initial position of valid pulses within dataframes
#Dataframes must be fed as an iterable object (list, tuple, np.array, etc.)
#Pulse polarity may be either +1 or -1 (positive/negative)
#Offset stands for the vertical correction according to ADC value
def findTriggerPositions(frame, derivativesCount, pulsePolarity, digitizingBits):
    
    validTriggerPositions = []
    
    # Turn pulses heads up if negative data is being fed
    if pulsePolarity < 1:
        offset = 2**digitizingBits - 1
    else:
        offset = 0
    
    
    #Input frame must be an iterable object, not comma separated
    if not type(frame) == np.ndarray:
        frame = np.array(frame)
    frame = frame*pulsePolarity + offset
    
    #A certain number of continuous positive derivatives 
    # prove the existence of a pulse
    
    positiveDerivativesCount = 0
    
    
    i = 0
    while(i < len(frame) - derivativesCount):
        if(derivative((frame[i],frame[i+1])) > 0):
            positiveDerivativesCount += 1
        else:
            positiveDerivativesCount = 0
        
        if positiveDerivativesCount == derivativesCount:
            if i > (DEAD_SAMPLES_BEFORE_TRIGGER + BASELINE_AVERAGE_SAMPLES):
                validTriggerPositions.append(i - derivativesCount)
            
            i += SAMPLES_BETWEEN_PULSES - derivativesCount - 1

        i += 1
            
    return validTriggerPositions



#Determines whether a pulse is valid or not
def validPulse(x, dropSaturated = DROP_SATURATED_PULSES, saturationValue = SATURATION_VALUE,
               triggerLocation = DEAD_SAMPLES_BEFORE_TRIGGER + BASELINE_AVERAGE_SAMPLES,
               windowSize = 300, thrshldRatios = 1.25):


    if not len(x):
        return 0

    #print("Checking if pulse is valid")

    if dropSaturated:
        if saturationValue in x:
            return 0

    x = np.array(x)
    bl = np.max(x)
    
    x = bl - x
    
    initStart = triggerLocation
    
    #Verify is baseline is really a representative value
    blAvg = sum(x[:initStart]) / initStart
    
    if blAvg > MAX_BL_DEVIATION:
        return 0
    
    
    return 1
    
    initEnd = initStart + windowSize
    
    endStart = initEnd
    endEnd = endStart + windowSize

    initAvg = sum(x[initStart:initEnd])
    endAvg = sum(x[endStart:endEnd]) + 0.1
    
    
    
    
    
    
    
    # print("Average ratios: " + str(initAvg / endAvg))
    
    if (endAvg < 0 and endAvg > 0):
        currentProportion = initAvg / endAvg
    else:
        currentProportion = 100    
    
    
    
    if (currentProportion) > thrshldRatios:
        return 1
    
    
    return 0



# Find peak positions within a single pulse. Returns list of peak positions
# relative to pulse (array) object index.
def pkDetect(x, deadTimeBefore, thr = 4, pkh = 2):
    peaks = []
    lMax=0
    iMax=0 
    
    for i in range(1,len(x)):
        if x[i] <= thr:
            pass
        elif x[i] > lMax:
            iMax=i
            lMax=x[i]
        elif ((lMax-x[i-1]) > pkh) and (x[i]-x[i-1])>0:
            peaks.append(iMax - deadTimeBefore)
            lMax=0
        else:
            pass
            
    return peaks



#Compute a single pulse's baseline. Returns basline value (float)
def computeBaseline(pulse, baselineSamples):
    
    #Convert list to numpy array
    if not type(pulse) == np.ndarray:
        pulse = np.array(pulse)
        
    bLine = np.average(pulse[:baselineSamples])
    
    return bLine
    

#Given a pulse (iterable object) and a baseline value (float),
#returns a integer-based iterable pulse object with the removed baseline
def removeBaseline(pulse, baselineValue, pulsePolarity = SIGNAL_POLARITY):
    
    #Convert list to numpy array
    if not type(pulse) == np.ndarray:
        pulse = np.array(pulse)
    
    #Useful either with positive or negative raw pulses
    pulse = (pulse - baselineValue) * pulsePolarity
    
    return pulse.astype(int)
    



#Returns a list of valid pulses (each pulse is a list by itself), as well as their
# basline-less version. Main input parameter is a raw frame (as an iterable object)
def getPulsesFromFrame(frame, derivativesCount, removeBl = True, pulseLength = PMT_PULSE_LENGTH,
                       pulsePolarity = -1, digitizingBits = 8):
    
    
    if(len(frame)):
        
        #Isolated pulses list
        pulsesList = []
        
        #Isolated pulses without baseline
        if removeBl:
            pulsesListWoBl = []
            
        triggerPositions = findTriggerPositions(frame, derivativesCount, pulsePolarity, digitizingBits)
        
        #print("Trigger positions: " + str(triggerPositions))
        for i in triggerPositions:
            currentPulse = frame[i - BASELINE_AVERAGE_SAMPLES - DEAD_SAMPLES_BEFORE_TRIGGER:i+pulseLength]
            
            if validPulse(currentPulse):
                #Raw pulses
                pulsesList.append(currentPulse)
                
                
                if removeBl:
                    #Pulses without baseline
                    currentPulseBaseLine = computeBaseline(currentPulse, BASELINE_AVERAGE_SAMPLES)
                    thisPulseWoBl = removeBaseline(currentPulse, currentPulseBaseLine)
                    pulsesListWoBl.append(thisPulseWoBl)
                
        if removeBl:
            return pulsesListWoBl
        return pulsesList        
    
    else:
        return []

    
def testBench():            
    dFileName = './test.lag'

    f = open(dFileName, 'r')

    currentFrame = []

    for line in f:
        
        if(PULSE_HEADER in line):
            
            #pulsesListWoBl = getPulsesFromFrame(currentFrame, CONTINUOUS_POSITIVE_DERIVATIVES, removeBaseline=False)
            # print(pulsesList)
            # for i in pulsesList:
            
            pulsesListWoBl = getPulsesFromFrame(currentFrame, CONTINUOUS_POSITIVE_DERIVATIVES, removeBaseline=True)
            for i in pulsesListWoBl:
                plt.plot(i)
            plt.show()
            currentFrame = []
            currentFrameName = line
            
            print(currentFrameName)
            
        else:
            currentFrame.append(int(line))


    f.close()    


#Given a directory to look for input files, returns a list of single
#valid pulses as individual lists within a list. Baseline removal is optional (default True)
def generatePulsesList(inputDir, inputFileExtension = INPUT_FILE_EXTENSION, removeBl = True):
    
    fileList = getFilesList(inputDir, inputFileExtension)   
    
    print("Files list: " + str(fileList))
    
    pulsesList = []
    pulseNamesList = []
    
    for currentFile in fileList:
    
        #For each of the files within the input directory
        print('Processing file: ' + str(currentFile))
        f = open(currentFile, 'r')
        currentFrame = [] 
        
        
        #Read line by line
        for line in f:
            
            #If a frame trigger is found
            if(PULSE_HEADER in line):
                               
                #Names will be preserved in the final Pandas DF
                currentFrameName = line.rstrip("\n")
                
                #Get a list of individual pulses (which are iterable objects themselves)
                pulses = getPulsesFromFrame(currentFrame, CONTINUOUS_POSITIVE_DERIVATIVES, removeBl = removeBl)
                
                
                #There may exist multiple pulse events within the same frame
                pulseNameCounter = 0
                
                #Let's analyse each single isolated pulse
                if len(pulses):
                    for i in pulses:
                        
                        #Add the pulse to our "clean" pulses list (already without baseline)
                        pulsesList.append(i) 
                        
                        #And associate a name to it
                        thisPulseName = currentFrameName + "_" + str(pulseNameCounter)
                        
                        
                        #print(thisPulseName)
                        
                        
                        
                        pulseNamesList.append(thisPulseName)
                        pulseNameCounter += 1
                
                #Let's clean the area for the next frame to be analyzed    
                currentFrame = []
                
            #Adding the raw pulse data (read from the current file) to the frame list
            else:
                currentFrame.append(int(line))
        
        #Close the current file
        f.close()
                        
    
    #After processing all the files in the directory, two separate lists will be returned:
        # The first list contains only the individual pulse names (list of strings)
        # The second list contains the clean pulse traces (list of numpy arrays)
        
    return pulseNamesList, pulsesList
            
    
            
#Generate Pandas DataFrame using individual pulse names and traces (without baseline)
def generatePulsesDf(pulseNames, pulsesWoBl):
    data = {}
    
    for i in range(len(pulseNames)):
        # data[pulseNames[i]] = pd.Series(pulsesWoBl[i].astype(int))
        data[i] = pd.Series(pulsesWoBl[i].astype(int))
    
    #Create dataframe
    df = pd.DataFrame(data, index = range(len(pulsesWoBl[0])))
    
    #Remove corrupt data traces
    df = df.dropna(axis = 1)
    
    # print(df)
        
    return df.T.astype(int)
    

#Given a Pandas Dataframe with pulses (each row is a single pulse)
#Returns the same dataframe with a new column, containing the peak locations of each pulse
def addPeakLocations(df):
    
    dfTmp = df.copy()
    dfTmp[PEAK_LOCATIONS_COLUMN_NAME] = dfTmp[dfTmp.columns[:PMT_PULSE_LENGTH]].apply(pkDetect, args = (DEAD_SAMPLES_BEFORE_TRIGGER + BASELINE_AVERAGE_SAMPLES, PK_THRSH_ABS, PK_HEIGHT), axis = 1)
    
    return dfTmp

def computeAreas(df, columnName = CURRENT_RADIONUCLEID):
    
    dfTmp = df.copy()
    
    dfAreas = pd.DataFrame()
    dfAreas[columnName] = dfTmp[dfTmp.columns[:PMT_PULSE_LENGTH]].sum(axis = 1)
    
    return dfAreas

#Given a Pandas Dataframe with pulses and a "PeakLocation" column
#Returns the filtered Pandas Data frame, where the "non-valid" pulses have been dropped
def cleanPulsesByPeakCriteria(df, minimumPulseCount, pulseCountWindow):
    dfOriginal = df.copy() 
    dfTmp = dfOriginal.iloc[:,:pulseCountWindow] #Create a temporary dataframe to filter by initial "windowsize" peak locations
    dfTmp = addPeakLocations(dfTmp)
    
    #Remove pulses that don't comply with the required peak-count criteria
    dfTmp = dfTmp.drop(dfTmp.loc[dfTmp[PEAK_LOCATIONS_COLUMN_NAME].map(len) < minimumPulseCount].T, axis = 0)
    
    #Return the filtered dataframe (with existing peaklocations) corresponding to the "nice pulses" indexes
    return dfOriginal.loc[dfTmp.index, :]









if __name__ == "__main__":
    # testBench()
    
    #ToDo: Generate the DataFrame (generatePulsesDf) (and store it as CSV) using the names and data from generatePulsesList
    #Generate a DF with the peak locations and discard pulses with less than X (3-10?) pulses in the first N (300?) samples
    
    names, pulses = generatePulsesList(INPUT_DIR, INPUT_FILE_EXTENSION)
 
    #Names and individual pulses MUST be the same length
    #Otherwise, something wrong happened
    if len(names) == len(pulses):     
        pulsesDf = generatePulsesDf(names, pulses)
        pulsesDf = cleanPulsesByPeakCriteria(pulsesDf, PK_DISTR_MIN_PULSES, PK_DISTR_WINDOW_SIZE)
        
        areasDf = computeAreas(pulsesDf, CURRENT_RADIONUCLEID)
        pulsesDf = addPeakLocations(pulsesDf)
        
        pulsesDf.to_clipboard()
        print(pulsesDf)
        
        print("Writing output files...")
        
        pulsesDf.to_csv(INDIVIDUAL_PULSES_FILE_NAME)
        areasDf.to_csv(AREAS_FILE_NAME)
        
        
        # for i in range(len(pulsesDf)):
        #     samplePulse = pulsesDf.iloc[i]
        #     onlyPulse = samplePulse.iloc[:-1]
            
        #     peaks = np.array(samplePulse.iloc[-1]) + DEAD_SAMPLES_BEFORE_TRIGGER + BASELINE_AVERAGE_SAMPLES
        #     plt.plot(onlyPulse)
        #     plt.plot(peaks, onlyPulse[peaks], 'r.')
        #     plt.show()
        
        
        
    else:
        print("FATAL ERROR. Individual pulse parsing failed!")
    
    
    